import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';
import { fabric } from 'fabric';
@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
  products:any={}
   tops:any;
   clothes:any
   gadget:any
   packs:any
   disps:any
   print:any
   details:any={}
   produit: any = [];
   packages:any=[]
   hide=false
   hidepack=false
   hidebody=true
   url='/editor/tops/'
  constructor(private l : ListService) { }

  ngOnInit(): void {
    this.l.getclothpage().subscribe(
      res=>{
        this.clothes = res.data
        for (let item of this.clothes) {
          item.description = JSON.parse(item.description);
          if(JSON.parse(item.objf)!=null){
            this.getCanvasUrl2(JSON.parse(item.objf), item)
            .then(async (res) => {
             })
            .catch((err) => {
              console.log(err);
             });
          }
         this.getCanvasUrl(JSON.parse(item.obj), item)
           .then(async (res) => {
            })
           .catch((err) => {
             console.log(err);
            });
            
       }
      
      },
      err=>{
        console.log(err)
      }
    )
   this.l.getpackpage().subscribe(
     res=>{
       console.log(res)
       this.packs=res.data
       for(let item of this.packs){
        item.description = JSON.parse(item.description);
        this.getCanvasUrl(JSON.parse(item.obj), item)
           .then(async (res) => {
            })
           .catch((err) => {
             console.log(err);
            });
        this.getCanvasUrl2(JSON.parse(item.objf), item)
           .then(async (res) => {
            })
           .catch((err) => {
             console.log(err);
            });
       }
     }
   )

    //this.l.gettops().subscribe(res=>{this.tops=res},err=>{console.log(err)});
  }

  async getCanvasUrl(obj: any, item: any) {
    var product:any
    let canvas = new fabric.Canvas(null, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful: true,
      stopContextMenu: false,
    });

    return await canvas.loadFromJSON(obj, (ob: any) => {
      canvas.setHeight(400);
      canvas.setWidth(400);
       if(JSON.parse(item.objf)!=null)
{
  product = {
    url: canvas.toDataURL(),
    url2:this.products.url2,
    price: item.description.price,
    promo: item.description.promo,
    size: item.description.size,
    type: item.description.type,
    name: item.description.name,
    owner: item.owner,
    made_with: item.description.made_with,
    item:item,
    width: item.width,
    height: item.height,
  };
  //console.log(product)
  if(item.category=="1"){
    this.produit.push(product);
  }
  if(item.category=="2"){
    this.packages.push(product);
    console.log(this.packages)
  } 
}else
if(
  JSON.parse(item.objf)==null
){
  product = {
    url: canvas.toDataURL(),
    url2:null,
    price: item.description.price,
    promo: item.description.promo,
    size: item.description.size,
    type: item.description.type,
    name: item.description.name,
    owner: item.owner,
    made_with: item.description.made_with,
    item:item,
    width: item.width,
    height: item.height,
  };
  //console.log(product)
  if(item.category=="1"){
    this.produit.push(product);
  }
  if(item.category=="2"){
    this.packages.push(product);
    console.log(this.packages)
  } 
}
        
      
    });
    
  }


  async getCanvasUrl2(objf: any, item: any) {

    let canvas = new fabric.Canvas(null, {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful: true,
      stopContextMenu: false,
    });

    return await canvas.loadFromJSON(objf,(ob: any) => {
      canvas.setHeight(400);
      canvas.setWidth(400);
      this.products = {
        url2:canvas.toDataURL(),
       
       
      };
      

        
      
    });
    
  }
  showDetails(data:any){
  
    Object.assign(this.details,{url:data.url,url2:data.url2,made_with:data.made_with,price:data.price,name:data.name,size:data.size,promo:data.promo, item:data.item, width:data.width, height:data.height})
    console.log(this.details)
    this.hide=true
    this.hidebody=false
    this.hidepack=false
   
  
  }
  showDetals(data:any){
  
    Object.assign(this.details,{url:data.url,url2:data.url2,made_with:data.made_with,price:data.price,name:data.name,size:data.size, item:data.item, width:data.width, height:data.height})
    console.log(this.details)
    this.hide=false
    this.hidebody=false
    this.hidepack=true
   
  
  }
}
