import { Component, OnInit, Output,EventEmitter ,Input} from '@angular/core';
import { ListService } from 'src/app/core';
@Component({
  selector: 'app-aladin-products-packs',
  templateUrl: './aladin-products-packs.component.html',
  styleUrls: ['./aladin-products-packs.component.scss']
})
export class AladinProductsPacksComponent implements OnInit {

  packs:any;
  @Input() id:any;
  @Input() name:any

  pack="packs";
 data:any={};
  current_pack:any=1;
  pages=[1,2,3,4,5];
  loading=false;
  
  @Output() newItemEvent =new EventEmitter<string>();
  constructor(private http:ListService) { }

  ngOnInit(): void {
    this.current_pack=this.pages[0];
    this.http.getPacks().subscribe(
      res=>{
     this.packs=res;
    },
    err=>{
      console.log(err);
    }
    );

  }
  onloading(){
    this.loading=!this.loading;
  }

  addNewItem(value: any) {
   Object.assign(this.data,{
    aladin: true,
    ​
    category: "packs",
    ​
    comment: value.comment,
    ​
    id: value.pack_id,
    ​
    name: value.name_packaging,
    ​
    price: value.price,
    ​
    qty: value.qty,​
    show: false,​
    size: value.dim,
    url: value.img,
    type:value.type,
    user:value.staff

   })
  
    this.newItemEvent.emit(this.data);
  }



  currentpack(event:any){
    let id=event.target.id
    this.current_pack=id
    this.http.getpacks(this.current_pack).subscribe(
      res=>{
        this.packs=res;
        console.log(res)
    })
  }
  
  nextpack(){
    if(this.current_pack!=undefined){
      this.current_pack=(+this.current_pack)+1
    }else{
      this.current_pack=1
    }
   
    this.http.getpacks(this.current_pack).subscribe(
     res=>{
       this.packs=res;
       console.log(res)
   })
  }
  
  
  
  previouspack(){
    if(this.current_pack !=undefined && this.current_pack>1){
      this.current_pack=(+this.current_pack)-1
      this.http.getpacks(this.current_pack).subscribe(
       res=>{
         this.packs=res;
         console.log(res);
     })
    
    }
    }

}
