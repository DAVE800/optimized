import { Component, OnInit,Input } from '@angular/core';
import { HttpService, ListService,LocalService,AladinService } from 'src/app/core';
import { NeweditorService } from 'src/app/core';
import { fabric } from 'fabric';
import { ColorEvent } from 'ngx-color';
declare var require :any
var $ = require("jquery")
@Component({
  selector: 'app-aladin',
  templateUrl: './aladin.component.html',
  styleUrls: ['./aladin.component.scss']
})
export class AladinComponent implements OnInit {

  constructor(private Editor:NeweditorService,private http:HttpService,private forms:ListService, private local:LocalService,private aladin:AladinService) {
   
   }
   pallette=false;
   produite=false;
   modeles=false
   formes=false
   product:any={}
  doubled=false
  colors=["blue","white","black","red","orange","green","#999999","#454545","#800080","#000080","#00FF00","#800000","brown","#2596be","#2596be","#be4d25"]
  current_page:any=1
  img:any
  model:any
  models:any=[]
  title = 'editor';
  file3:any
  viewimage:any
  canvas:any
  width=511;
  height=300;
  testingpicture:any;
  text:any;
  products:any=[]
  state:any=[];
  undo:any=[];
  redo:any=[];
  text40:any
  cpt=0;
  mods=0;
  textalign=["left","center","justify","right"]
  hastext=false
  cacheimg=true
  cacheimg1=true
  clipart:any
  produit=false
  texte=false
  forme=false
  modele=false
  @Input() data:any
  prourl2:any
  origin_1=null;
  origin_2=null;
  face1:any
  face2:any
  text_width=38
  newleft=0;
  isRedoing=false;
  fonts = [
  {name:"Flowerheart",url:"./assets/fonts/FlowerheartpersonaluseRegular-AL9e2.otf"},
  {name:"HussarBold",url:"./assets/fonts/HussarBoldWebEdition-xq5O.otf"},
  {name:'Branda',url:"./assets/fonts/Branda-yolq.ttf"},
  {name:"MarginDemo",url:"./assets/fonts/MarginDemo-7B6ZE.otf"},
  {name:"Kahlil",url:"./assets/fonts/Kahlil-YzP9L.ttf"},
  {name:"FastHand",url:"./assets/fonts/FastHand-lgBMV.ttf"},
  {name:"BigfatScript",url:"./assets/fonts/BigfatScript-jE96G.ttf"},
  {name:"Atlane",url:"./assets/fonts/Atlane-PK3r7.otf"},
  {name:"HidayatullahDemo",url:"./assets/fonts/Bismillahscript-4ByyY.ttf"},
  {name:"Robus",url:"./assets/fonts/HidayatullahDemo-mLp39.ttf"},
  {name:"Backslash",url:"./assets/fonts/Robus-BWqOd.otf"},
  {name:"ChristmasStory",url:"./assets/fonts/ChristmasStory-3zXXy.ttf"},
];

 
  Uplade(event:any){ 
  this.file3 =event.target.files[0] 
  const reader = new FileReader();
  reader.onload = () => {
   this.viewimage= reader.result;
   this.cacheimg=false
   this.cacheimg1=true
  };
  reader.readAsDataURL(this.file3);
   
   }

uplod(event:any){
  this.Uplade(event)
 }


 ngOnInit():void{
this.data.head=false
this.data.editor=true
  this.forms.getclipart(this.current_page).subscribe(res=>{
    this.clipart=res;
  this.clipart=this.clipart.cliparts;
},
er=>{console.log(er)})

  this.canvas= new fabric.Canvas('aladin',{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false,
    
  });
  
  this.canvas.filterBackend=new fabric.WebglFilterBackend(); 
  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);
  this.canvas.on('object:modified',() =>{
   
    
  });

  this.canvas.on("object:added",()=>{

    if(!this.isRedoing){
      this.state = [];
    }
    this.isRedoing = false;
  })

  this.canvas.on("object:created",(e:any)=>{

  })
 this.canvas.on("mouse:move",(e:any)=>{
  // this.contextmenu(e)
 })

  this.canvas.on("mouse:dblclick",(e:any)=>{
  if(this.canvas.getActiveObject().isType('image')){
    this.aladin.triggerMouse(document.getElementById("importing"));
      this.doubled=true
    }
    
  })

  this.canvas.on("selection:updated",(e:any)=>{

  })

  setTimeout(()=>
  {
    this.canvas.loadFromJSON(JSON.parse(this.data.item.obj),()=>{
      this.canvas.setWidth(this.data.width)
      this.canvas.setHeight(this.data.height)
      this.face1=this.canvas.toDataURL();
      if(this.data.url2!=null){
        this.face2=this.data.url2;

      }
      this.canvas.requestRenderAll()

    })
  },20)
 
  this.http.get().subscribe(
    res=>{
      this.model=res
      for(let item of this.model){
      this.model[this.model.indexOf(item)].description = JSON.parse(item.description);
      if(this.aladin.IsJsonString(item.obj)){
        this.getCanvasUrl(JSON.parse(this.model[this.model.indexOf(item)].obj),item).then(async (res)=>{
            }).catch((err)=>{ 
              console.log(err)
            })
     
      }
     
      }

     }
  )

 }

 async getCanvasUrl(obj:any,item:any){
  let canvas= new fabric.Canvas(null,{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false,
  });

 return await  canvas.loadFromJSON(obj,(ob:any)=>{
  canvas.setHeight(item.height)
  canvas.setWidth(item.width)
   var product={
    url:canvas.toDataURL(),
    price:item.description.price,
    promo:item.description.promo,
    type:item.description.type,
    name:item.description.name,
    owner:item.owner,
    item:item
  }
  if(item.description.type=="model"){
   this.models.push(product);
  }

  if(item.description.type=="produits"){
    this.products.push(product)
  }
  
 })
}

//url2
async getCanvasUrl2(item:any){
  let canvas= new fabric.Canvas(null,{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking:true,
    stateful:true,
    stopContextMenu:false,
  });
  canvas.setHeight(item.height)
  canvas.setWidth(item.width)
  return await  canvas.loadFromJSON(JSON.parse(item.objf),(ob:any)=>{
  this.face2=canvas.toDataURL()
 })

}

loadCanvas(item:any){
    this.face2=""
    this.canvas.loadFromJSON(item.obj,()=>{
    this.canvas.setWidth(item.width)
    this.canvas.setHeight(item.height)
    this.face1=this.canvas.toDataURL()
    this.canvas.requestRenderAll()
      if(JSON.parse(item.objf)!=null){
          this.getCanvasUrl2(item)
        
      }  
  })

}
 makeItalic(){
  this.Editor.italic(this.canvas)
}

Redo(){
if(this.state.length>0){
  this.isRedoing = true;
 this.canvas.add(this.state.pop());
}

}

Undo(){
if(this.canvas._objects.length>0){
  this.state.push(this.canvas._objects.pop());
  this.canvas.renderAll();
 }

 
}

makeBold(){
  this.Editor.bold(this.canvas)   
}

underlineText(){
  this.Editor.underline(this.canvas)
}

sendBack(){
  this.Editor.sendBack(this.canvas)
}

sendForward(){
  this.Editor.sendForward(this.canvas)
}
overlineText(){
  this.Editor.overline(this.canvas)
}

addText(){
  if(!this.hastext){
    this.Editor.addText(this.canvas);
    this.hastext=true;
  }
}

duplicate(){
  this.copy();
  this.paste()
}

copy(){
  this.Editor.copy(this.canvas)
}

paste(){
  this.Editor.paste(this.canvas)
}


save(savehistory:Boolean) {
  if (savehistory === true) {
    let myjson = JSON.stringify(this.canvas.toJSON()); 
    this.state.push(myjson);
}  
}


textAlign(val:any){
  this.Editor.textAlign(this.canvas,val)

}

removeItem(){
  this.Editor.remove(this.canvas);
}

textfont(item:any){
  let data= item.target.value
  console.log(data.substr(0,data.indexOf("*")))
  this.Editor.textfont(Object.assign({},{name:data.substr(0,data.indexOf("*")),url:data.substr(data.indexOf("*")+1,data.length-1)}),this.canvas)
}

InputChange(Inputtext:any){
  if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){
    if(this.cpt==0){
      this.text=this.canvas.getActiveObject().text+" "+ this.text
      this.cpt=this.cpt+1
      this.canvas.getActiveObject().text= this.text
      this.canvas.requestRenderAll();
    }else{
      this.canvas.getActiveObject().text= this.text
      this.canvas.requestRenderAll();
    }

  }else{  
    let text= new fabric.Textbox(this.text,{
      top:200,
      left:200,
      fill:"blue",
      fontSize:38,
      fontStyle:'normal',
      cornerStyle:'circle',
      selectable:true,
      borderScaleFactor:1,
      overline:false,
      lineHeight:1.5
    });
    this.canvas.add(text).setActiveObject(text);
    this.canvas.renderAll(text);
    this.canvas.requestRenderAll();
    this.canvas.centerObject(text);
  }


}

texteclor($event:ColorEvent){
this.Editor.textcolor($event.color.hex,this.canvas);
}


setItem(event:any){
this.Editor.setitem(event,this.canvas)
}


onFileUpload(event:any){
  let file = this.file3;
  this.cacheimg=true
  this.cacheimg1=false
  if(!this.Editor.handleChanges(file)){
  if(!this.doubled){
    const reader = new FileReader();
  reader.onload = () => {
    let url:any = reader.result;
    fabric.Image.fromURL(url,(oImg) =>{
    oImg.set({
        scaleX:0.5,
        scaleY:0.5,
        crossOrigin: "Anonymous",
  });
    this.canvas.add(oImg).setActiveObject(oImg);
    this.canvas.centerObject(oImg);
    this.canvas.renderAll(oImg)

  })
    };
    reader.readAsDataURL(file);

  }else{
    var width=this.canvas.getActiveObject().width
    var height=this.canvas.getActiveObject().height
    var left=this.canvas.getActiveObject().left
    var top=this.canvas.getActiveObject().top
    this.aladin.remove(this.canvas);
    //this.save(true)
    const reader = new FileReader();
    reader.onload = () => {
    let url:any = reader.result;
    fabric.Image.fromURL(url,(oImg) =>{
    oImg.set({
        scaleX:0.5,
        scaleY:0.5,
        crossOrigin: "Anonymous",
        left:left,
        top:top,
        height:height,
        width:width
  });
    this.canvas.insertAt(oImg,1,false).setActiveObject();
    this.canvas.centerObject(oImg);
    this.canvas.renderAll(oImg)
    this.canvas.requestRenderAll();
    this.sendForward()
    this.doubled=false;
  })
    };
    reader.readAsDataURL(file);
  }
   
  }

}

InputSize(){
  var canvasWrapper:any = document.getElementById('wrapper');
  canvasWrapper.style.width = this.width;
canvasWrapper.style.height = this.height;
this.canvas.setWidth(this.width);
this.canvas.setHeight(this.width);
}

textwidth(){
if(this.canvas.getActiveObject().text){
  console.log(this.canvas.getActiveObject())
  this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height+1})
  this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width+1})
  this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize+1})
  this.text_width= this.canvas.getActiveObject().fontSize
  this.canvas.renderAll()
}

}

minus(){
  if(this.canvas.getActiveObject().text){
    console.log(this.canvas.getActiveObject())
    this.canvas.getActiveObject().set({height:this.canvas.getActiveObject().height-1})
    this.canvas.getActiveObject().set({width:this.canvas.getActiveObject().width-1})
    this.canvas.getActiveObject().set({fontSize:this.canvas.getActiveObject().fontSize-1})
    this.text_width= this.canvas.getActiveObject().fontSize
    this.canvas.renderAll()
  }
  
}

currentPage(event:any){
  var page=event.target.id
    if(+page){
      this.current_page=page
      
      this.forms.getclipart(page).subscribe(res=>{
        let data:any=res;
        if(data.status==200){
          if(data.cliparts.length>0){
            
            this.clipart=data.cliparts
          }else{
           
          }
        
        }
      }, )
    }

}

Savemodal(){
  this.face1=this.canvas.toDataURL()
  this.face2=this.data.url2
  let data:any={}
  Object.assign(data,{face1:this.face1,face2:this.face2,data:this.data});
 
  let cart_data={
    qty:"",
    price:"" ,
    t: "" ,
    id:"",
    face1:"",
    face2:"", 
    shape:"",
    type:"",
    size:"", 
    type_product:"editor",
    category:"clothes"

  };
}

contextmenu(event:any){
  let item =this.canvas.getActiveObject()
  if(item.type=="textbox" && item){
 
  }
  return false
}

showpallette(){
  this.pallette=true;
   this.produite=false;
   this.modeles=false
   this.formes=false
}

showforme(){
  this.pallette=false;
   this.produite=false;
   this.modeles=false
   this.formes=true
}
showmodeles(){
  this.pallette=false;
   this.produite=false;
   this.modeles=true
   this.formes=false
}
showproduite(){
  this.pallette=false;
   this.produite=true;
   this.modeles=false
   this.formes=false
}

}
