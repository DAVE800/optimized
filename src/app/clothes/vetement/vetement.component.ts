import { createAttribute } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { charAtIndex } from 'pdf-lib';
import { ListService ,LocalService, AladinService, HttpService} from 'src/app/core';
import { fabric } from 'fabric';
declare  var require:any;
var myalert=require('sweetalert2');
var $ = require("jquery");
@Component({
  selector: 'app-vetement',
  templateUrl: './vetement.component.html',
  styleUrls: ['./vetement.component.scss']
})
export class VetementComponent implements OnInit {
 cltobj:any=[]
 file:any
 imagepreview:any
 CacheCrea=false
 cacheClothes=true;
 details:any={};
 showdetail=false;
 models:any
 url="/editor/cloth/"
 obj:any=[]
 objf:any=[]
 cart=false;
 canvas:any
 products:any
 produit:any=[]
 prod:any={}
 prodcloths:any={}
 data="Vêtement : Aladin vous propose des vêtements personnalisés de très bonne qualité avec des finitions qui respectent vos goûts. Vous serez fière de les porter partout quel que soit le lieu et l’événement."
 constructor(private p:ListService,private local:LocalService, private uplod:AladinService, private http:HttpService) { }

  ngOnInit(): void {
    this.http.get().subscribe(res=>{
      this.models=res
      for(let item of this.models){
      item.description = JSON.parse(item.description);
     this.getCanvasUrl2(item).then((res)=>{

     }).catch((err)=>{console.log(err)})
      this.getCanvasUrl(JSON.parse(item.obj),item).then(async (res)=>{
      
        }).catch((err)=>{ 
         console.log(err)
       })
     }
      
    },
    err=>{
      console.log(err)
    })

  }

  async getCanvasUrl(obj:any,item:any){ 
    var product:any 
    let canvas= new fabric.Canvas(null,{
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor:'blue',
      fireRightClick: true,
      preserveObjectStacking: true,
      stateful:true,
      stopContextMenu:false,
  
    });
     return await  canvas.loadFromJSON(obj,(ob:any)=>{
       if(JSON.parse(item.objf)!=null){
        canvas.setHeight(item.height)
        canvas.setWidth(item.width)
          product={
          url:canvas.toDataURL(),
          url2:this.prodcloths.url2,
          price:item.description.price,
          promo:item.description.promo,
          size:item.description.size,
          type:item.description.type,
          name:item.description.name,
          owner:item.description.owner,
          comment:item.description.made_with,
          item:item,
          width:item.width,
          height: item.height
    
          
        }     
        if(item.category=="1"){
         this.produit.push(product);  
        }
       }
       if(JSON.parse(item.objf)==null){
        canvas.setHeight(item.height)
        canvas.setWidth(item.width)
          product={
          url:canvas.toDataURL(),
          url2:null,
          price:item.description.price,
          promo:item.description.promo,
          size:item.description.size,
          type:item.description.type,
          name:item.description.name,
          owner:item.description.owner,
          comment:item.description.made_with,
          item:item,
          width:item.width,
          height: item.height
          
        }     
        if(item.category=="1"){
         this.produit.push(product);  
        }
       }
           
      })
  }

  IsJsonString(str:any) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  async getCanvasUrl2(item:any){
     if(item !=null && JSON.parse(item.objf)!=null){
      let canvas= new fabric.Canvas(null,{
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor:'blue',
        fireRightClick: true,
        preserveObjectStacking: true,
        stateful:true,
        stopContextMenu:false,
  
      });
      return await  canvas.loadFromJSON(JSON.parse(item.objf),(ob:any)=>{
      canvas.setHeight(item.height);
      canvas.setWidth(item.width);
       this.prodcloths={
        url2:canvas.toDataURL(),
        
      }     
    
       
       
     })

     }else{
       return
     }
   
  
  }
 

  
 Upload(event:any){
   let file =event.target.files[0]
   if(!this.uplod.UpleadImage(file)){
   const reader = new FileReader();
 reader.onload = () => { 
  this.imagepreview = reader.result;
  
  };
  
  reader.readAsDataURL(file);
  this.affichecrea()
 
   console.log(event)
   }else{
     
   }
 }

showDetails(data:any){
  Object.assign(this.details,{url:data.url,url2:data.url2,made_with:data.made_with,price:data.price,name:data.name,size:data.size,show:true, item:data.item, width:data.width, height:data.height})
  console.log(this.details)
  this.CacheCrea=false
  this.cacheClothes=false;
 

}

affichecrea(){
  this.CacheCrea=true
  this.cacheClothes=false
}


getComponent(value:boolean){
  this.cacheClothes=value;
  this.CacheCrea=!this.CacheCrea;

}

ChangeComponent(value:boolean){
  this.cacheClothes=value
  
}

}
